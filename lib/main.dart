import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:namer_app/src/modal/NotificationData.dart';
import 'package:namer_app/src/modal/ProductData.dart';
import 'package:namer_app/src/routing/Routes.dart';
import 'package:namer_app/src/screens/admin/link-registers/LinkRegisterScreen.dart';
import 'package:namer_app/src/screens/admin/user-managements/UserManagementScreen.dart';
import 'package:namer_app/src/screens/client/HomeScreen.dart';
import 'package:namer_app/src/screens/client/Profile.dart';
import 'package:namer_app/src/screens/client/notification/NotificationScreen.dart';
import 'package:namer_app/src/screens/client/phone-books/PhoneBookScreen.dart';
import 'package:go_router/go_router.dart';


final router = GoRouter(
   routes: [
    //  GoRoute(
    //    path: '/',
    //    builder: (context, state) => HomeScreen(),
    //  ),
    //  GoRoute(
    //   path: '/phone-book',
    //   builder: (context, state) => const PhoneBook(
    //       products: [
    //         Product(name: 'Eggs'),
    //         Product(name: 'Flour'),
    //         Product(name: 'Chocolate chips'),
    //       ],
    //     ),
    //   ),
    //   GoRoute(
    //     path: '/user-management',
    //     builder: (context, state) => const UserManagement(),
    //   ),
    //   GoRoute(
    //     path: '/link-register',
    //     builder: (context, state) => const LinkRegister(),
    //   ),
    //   GoRoute(
    //     path: '/notification',
    //     builder: (context, state) => const NotificationScreen(
    //       notif: [
    //         NotificationData(id : "1",name: 'Eggs',title: 'Thông báo có cuộc gọi mới', description: 'Bạn nhận được cuộc gọi từ NDP'),
    //         NotificationData(id : "2",name: 'Demo',title: 'Thông báo có cuộc gọi mới', description: 'Bạn nhận được cuộc gọi từ NDP'),
    //         NotificationData(id : "3",name: 'Ãdsd',title: 'Thông báo có cuộc gọi mới', description: 'Bạn nhận được cuộc gọi từ NDP'),
    //       ],),
    //   ),
    //   GoRoute(
    //     path: '/profile',
    //     builder: (context, state) => const UserProfile(),
    //   ),
   ],
 );
//  runApp(MaterialApp.router(routerConfig: router)
void main() {
  runApp(GetMaterialApp(
    debugShowCheckedModeBanner: false,
    initialRoute: AppPage.getnavbar(),
    getPages: AppPage.routes,
  ));
}


