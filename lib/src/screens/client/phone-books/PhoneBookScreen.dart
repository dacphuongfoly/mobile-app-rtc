import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:namer_app/src/routing/FooterNavigation.dart';
import 'package:namer_app/src/components/phone-books/ListPhoneBook.dart';

import '../../../modal/ProductData.dart';
typedef CartChangedCallback = Function(Product product, bool inCart);

class PhoneBook extends StatefulWidget {
  const PhoneBook({required this.products, super.key});

  final List<Product> products;

  @override
  State<PhoneBook> createState() => _ShoppingListState();
}

class _ShoppingListState extends State<PhoneBook> {
  final _shoppingCart = <Product>{};

  void _handleCartChanged(Product product, bool inCart) {
    setState(() {
      if (!inCart) {
        _shoppingCart.add(product);
      } else {
        _shoppingCart.remove(product);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
      ),
      body: Container(
        child: Column(
          children: [
            Expanded(child: ListPhoneBook()),
            // Expanded(
            //   child: ListView(
            //       padding: const EdgeInsets.symmetric(vertical: 8.0),
            //       children: widget.products.map((product) {
            //         return ShoppingListItem(
            //           product: product,
            //           inCart: _shoppingCart.contains(product),
            //           onCartChanged: _handleCartChanged,
            //         );
            //       }).toList()),
            // ),
            // Expanded(child: _buildCard())
          ],
        ),
      ),
    );
  }
}

class ShoppingListItem extends StatelessWidget {
  ShoppingListItem({
    required this.product,
    required this.inCart,
    required this.onCartChanged,
  }) : super(key: ObjectKey(product));

  final Product product;
  final bool inCart;
  final CartChangedCallback onCartChanged;

  Color _getColor(BuildContext context) {
    return inCart //
        ? Colors.black54
        : Theme.of(context).primaryColor;
  }

  TextStyle? _getTextStyle(BuildContext context) {
    if (!inCart) return null;

    return const TextStyle(
      color: Colors.black54,
      decoration: TextDecoration.lineThrough,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        onCartChanged(product, inCart);
      },
      leading: CircleAvatar(
        backgroundColor: _getColor(context),
        child: Text(product.name[0]),
      ),
      title: Text(
        product.name,
        style: _getTextStyle(context),
      ),
    );
  }
}

Widget _buildCard() {
  return SizedBox(
    height: 210,
    child: Card(
      child: Column(
        children: [
          ListTile(
            title: const Text(
              '1625 Main Street',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            subtitle: const Text('My City, CA 99984'),
            leading: Icon(
              Icons.restaurant_menu,
              color: Colors.blue[500],
            ),
          ),
          const Divider(),
          ListTile(
            title: const Text(
              '(408) 555-1212',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            leading: Icon(
              Icons.contact_phone,
              color: Colors.blue[500],
            ),
          ),
          ListTile(
            title: const Text('costa@example.com'),
            leading: Icon(
              Icons.contact_mail,
              color: Colors.blue[500],
            ),
          ),
        ],
      ),
    ),
  );
}