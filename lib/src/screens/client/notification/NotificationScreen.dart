import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../modal/NotificationData.dart';

typedef NotifChangedCallback = Function(
    NotificationData notification, bool inCart);

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({required this.notif, super.key});

  final List<NotificationData> notif;

  @override
  State<NotificationScreen> createState() => _NotifListState();
}

class _NotifListState extends State<NotificationScreen> {
  final _onTapNotif = <NotificationData>{};
  TextEditingController notif = TextEditingController();

  void _handleNotifiChanged(NotificationData notification, bool inView) {
    setState(() {
      if (!inView) {
        _onTapNotif.add(notification);
      } else {
        _onTapNotif.remove(notification);
      }
    });
  }

  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
      ),
      body: ListView(
        children: [
          NotifListComp(
            title:
                "Congue eget elementum amvulput eget ementum amvulput eget ementum amvulput eget elementum amvul eget elementum amvulate tget elemente tget elementum amet vulputate ",
          ),
          NotifListComp(
            title: "Congue eget eluate tget elementum amet vulputate ",
          ),
          NotifListComp(
            title: "te tget elementum amet vulputate ",
          ),
          NotifListComp(
            title:
                "Congue eget elementum amvulputate tget elementum amet vulpuate tget elementum amet vulputate ",
          ),
          NotifListComp(
            title:
                "Congue eget el elementum amet vulpuate tget elementum a vulputate ",
          ),
          NotifListComp(
            title:
                "Congue eget elementum amvulput eget ementum amvulput eget ementum amvulput eget elementum amvul eget elementum amvulate tget elemente tget elementum amet vulputate ",
          ),
          NotifListComp(
            title: "Congue eget eluate tget elementum amet vulputate ",
          ),
          NotifListComp(
            title: "te tget elementum amet vulputate ",
          ),
          NotifListComp(
            title:
                "Congue eget elementum amvulputate tget elementum amet vulpuate tget elementum amet vulputate ",
          ),
          NotifListComp(
            title:
                "Congue eget el elementum amet vulpuate tget elementum a vulputate ",
          ),
          NotifListComp(
            title:
                "Congue eget elementum amvulput eget ementum amvulput eget ementum amvulput eget elementum amvul eget elementum amvulate tget elemente tget elementum amet vulputate ",
          ),
          NotifListComp(
            title: "Congue eget eluate tget elementum amet vulputate ",
          ),
          NotifListComp(
            title: "te tget elementum amet vulputate ",
          ),
          NotifListComp(
            title:
                "Congue eget elementum amvulputate tget elementum amet vulpuate tget elementum amet vulputate ",
          ),
          NotifListComp(
            title:
                "Congue eget el elementum amet vulpuate tget elementum a vulputate ",
          ),
        ],
      ),
    );
  }
}

class NotifListComp extends StatelessWidget {
  const NotifListComp({
    required this.title,
    super.key,
  });
  final title;

  void _handleOnClick() {
    print('More');
  }

  void _handleOnClickNotif() {
    print('Notif');
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
      child: SizedBox(
        height: 70,
        child: Row(
          children: [
            Image.asset(
              'assets/images/avt-image.png',
              fit: BoxFit.cover,
              width: 50,
              height: 50,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: SizedBox(
                  height: 80,
                  child: ListTile(
                    onTap: () => _handleOnClickNotif(),
                    title: Text(
                      title,
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        overflow: TextOverflow.ellipsis,
                      ),
                      maxLines: 2,
                    ),
                    subtitle: const Text('5m'),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 0,
              child: Padding(
                  padding: const EdgeInsets.all(0),
                  child: IconButton(
                    onPressed: () => _handleOnClick(),
                    icon: Icon(Icons.more_horiz),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
