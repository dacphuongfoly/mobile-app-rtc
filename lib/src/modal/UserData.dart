// ignore_for_file: file_names

class User {
  const User({
    required this.fullName,
    required this.username,
    required this.phoneNumber,
    required this.password,
    required this.imei,
    required this.lookAt,
    });

  final String fullName;
  final String username;
  final String phoneNumber;
  final String password;
  final String imei;
  final String lookAt;
}