class NotificationData {
  const NotificationData({
    required this.id,
    required this.name,
    required this.title,
    required this.description,
    });

  final String id;
  final String name;
  final String title;
  final String description;
}