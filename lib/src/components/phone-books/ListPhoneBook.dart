import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ListPhoneBook extends StatelessWidget {
  const ListPhoneBook({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        _PhoneContact('Nguyễn Đắc Phương', '85 W Portal Ave', Icons.theaters),
        _PhoneContact('The Castro Theater', '429 Castro St', Icons.theaters),
        _PhoneContact('AMC Metreon 16', '135 4th St #3000', Icons.theaters),
        const Divider(),
        _PhoneContact('Đắc Phương Nguyễn', '757 Monterey Blvd', Icons.restaurant),
        const Divider(),
        _PhoneContact('La Ciccia', '291 30th St', Icons.restaurant),
        _PhoneContact('La Ciccia', '291 30th St', Icons.restaurant),
      ],
    );
  }
}
ListTile _PhoneContact(String title, String subtitle, IconData icon) {
  
  return ListTile(
    onTap: () {
      print(title);
    },
    title: Text(title,
        style: const TextStyle(
          fontWeight: FontWeight.w500,
          fontSize: 20,
        )),
    subtitle: Text(subtitle),
    leading: Icon(
      icon,
      color: Colors.blue[500],
    ),
  );
}