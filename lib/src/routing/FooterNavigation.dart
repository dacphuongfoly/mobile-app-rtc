import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:namer_app/src/screens/admin/link-registers/LinkRegisterScreen.dart';
import 'package:namer_app/src/screens/client/HomeScreen.dart';
import 'package:namer_app/src/screens/client/Profile.dart';
import 'package:namer_app/src/screens/client/notification/NotificationScreen.dart';
import 'package:namer_app/src/screens/client/phone-books/PhoneBookScreen.dart';


class FooterNavigation extends StatefulWidget {
    FooterNavigation({
    Key ? key,
    required this.currentIndex ,
  }): super(key : key);
  int currentIndex;
  @override
  State<FooterNavigation> createState() => _FooterNavigationState();
}

class _FooterNavigationState extends State<FooterNavigation> {

@override
Widget build(BuildContext context) {
return Row(
  children: [
    Expanded(
      child: FloatingActionButton(
        heroTag: 'btn1',
      onPressed: () {
        context.go('/');
      },
      tooltip: 'Home',
      backgroundColor: Color(0xffffffff),
      child: SvgPicture.asset(
          '../../../assets/images/icons/home-ic.svg',
        ),
    )
    ),
    Expanded(
      child: FloatingActionButton(
        heroTag: 'btn2',
        onPressed: () {
          context.go('/phone-book');
        },
        tooltip: 'Phonebook',
        child: const Icon(Icons.phone_android),
      ),
    ),
    // Expanded(
    //   child: FloatingActionButton(
    //     onPressed: () {
    //       context.go('/user-management');
    //     },
    //     tooltip: 'User management',
    //     child: const Icon(Icons.manage_accounts_outlined),
    //   ),
    // ),
    Expanded(
      child: FloatingActionButton(
        heroTag: 'btn3',
        onPressed: () {
          context.go('/link-register');
        },
        tooltip: 'Link register',
        child: const Icon(Icons.app_registration_rounded),
      ),
    ),
    Expanded(
      child: FloatingActionButton(
        heroTag: 'btn4',
        onPressed: () {
          context.go('/notification');
        },
        tooltip: 'Notification',
        child: const Icon(Icons.notifications_outlined),
      ),
    ),
    Expanded(
      child: FloatingActionButton(
        heroTag: 'btn5',
        onPressed: () {
          context.go('/profile');
        },
        tooltip: 'Profile',
        child: const Icon(Icons.account_circle),
      ),
    ),
  ],
);
  }
}

// class FooterNavigation extends StatelessWidget {
//   FooterNavigation({
//     Key ? key,
//     required this.currentIndex ,
//   }): super(key : key);
  
//   int currentIndex = 0;

//   @override
//   Widget build(BuildContext context) {
//     return Row(
//       children: [
//         Expanded(
//           child: FloatingActionButton(
//             heroTag: 'btn1',
//           onPressed: () {
//             context.go('/');
//           },
//           tooltip: 'Home',
//           backgroundColor: Color(0xffffffff),
//           child: SvgPicture.asset(
//               '../../../assets/images/icons/home-ic.svg',
//             ),
//         )
//         ),
//         Expanded(
//           child: FloatingActionButton(
//             heroTag: 'btn2',
//             onPressed: () {
//               context.go('/phone-book');
//             },
//             tooltip: 'Phonebook',
//             child: const Icon(Icons.phone_android),
//           ),
//         ),
//         // Expanded(
//         //   child: FloatingActionButton(
//         //     onPressed: () {
//         //       context.go('/user-management');
//         //     },
//         //     tooltip: 'User management',
//         //     child: const Icon(Icons.manage_accounts_outlined),
//         //   ),
//         // ),
//         Expanded(
//           child: FloatingActionButton(
//             heroTag: 'btn3',
//             onPressed: () {
//               context.go('/link-register');
//             },
//             tooltip: 'Link register',
//             child: const Icon(Icons.app_registration_rounded),
//           ),
//         ),
//         Expanded(
//           child: FloatingActionButton(
//             heroTag: 'btn4',
//             onPressed: () {
//               context.go('/notification');
//             },
//             tooltip: 'Notification',
//             child: const Icon(Icons.notifications_outlined),
//           ),
//         ),
//         Expanded(
//           child: FloatingActionButton(
//             heroTag: 'btn5',
//             onPressed: () {
//               context.go('/profile');
//             },
//             tooltip: 'Profile',
//             child: const Icon(Icons.account_circle),
//           ),
//         ),
//       ],
//     );
//   }
// }
//   return BottomNavigationBar(
  //       currentIndex: _currentIndex,
  //       onTap: (index) {
  //         print(index);
  //         if (index == 0) {
  //           Navigator.push(
  //             context,
  //             MaterialPageRoute(builder: (context) => const HomeScreen()),
  //           );
  //         } else if (index == 1) {
  //           Navigator.push(
  //             context,
  //             MaterialPageRoute(builder: (context) => const PhoneBook(
  //               products: [

  //               ],
  //             )),
  //           );
  //         } else if (index == 2) {
  //           Navigator.push(
  //             context,
  //             MaterialPageRoute(builder: (context) => const LinkRegister()),
  //           );
  //         } else if (index == 3) {
  //           Navigator.push(
  //             context,
  //             MaterialPageRoute(builder: (context) => const NotificationScreen(
  //               notif: [],
  //             )),
  //           );
  //         }
  //         else if (index == 4) {
  //           Navigator.push(
  //             context,
  //             MaterialPageRoute(builder: (context) => const UserProfile()),
  //           );
  //         }
  //       },
  //       backgroundColor: Colors.white,
  //       type: BottomNavigationBarType.fixed,
  //       items: const <BottomNavigationBarItem>[
  //         BottomNavigationBarItem(
  //             icon: Icon(
  //               Icons.home_filled,
  //               color: Colors.blue,
  //             ),
  //             activeIcon: Icon(
  //               Icons.home,
  //               color: Colors.blue,
  //             ),
  //             label: "Home"),
  //         BottomNavigationBarItem(
  //             icon: Icon(
  //               Icons.view_list,
  //               color: Colors.blue,
  //             ),
  //             label: "Phonebook"),
  //         // BottomNavigationBarItem(
  //         //     icon: Icon(
  //         //       Icons.person,
  //         //       color: Colors.black,
  //         //     ),
  //         //     label: "User management"),
  //         BottomNavigationBarItem(
  //             icon: Icon(
  //               Icons.person,
  //               color: Colors.blue,
  //             ),
  //             label: "Linkregister"),
  //         BottomNavigationBarItem(
  //             icon: Icon(
  //               Icons.notifications,
  //               color: Colors.blue,
  //             ),
  //             label: "Notification"),
  //         BottomNavigationBarItem(
  //             icon: Icon(
  //               Icons.person,
  //               color: Colors.blue,
  //             ),
  //             label: "Profile"),
  //       ]);
  // }