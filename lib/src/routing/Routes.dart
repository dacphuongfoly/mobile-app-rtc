import 'package:get/get.dart';
import 'package:namer_app/src/navbar/Navbar.dart';
import 'package:namer_app/src/screens/admin/link-registers/LinkRegisterScreen.dart';
import 'package:namer_app/src/screens/admin/user-managements/UserManagementScreen.dart';
import 'package:namer_app/src/screens/client/HomeScreen.dart';
import 'package:namer_app/src/screens/client/Profile.dart';
import 'package:namer_app/src/screens/client/phone-books/PhoneBookScreen.dart';

class AppPage{
  static List<GetPage> routes = [
    GetPage(name: home, page: () => HomeScreen()),
    GetPage(name: navbar, page: () => NavbarScreen()),
    GetPage(name: phoneBook, page: () => PhoneBook(products: [],)),
    GetPage(name: linkRgt, page: () => LinkRegister()),
    GetPage(name: userManagement, page: () => UserManagement()),
    GetPage(name: notification, page: () => HomeScreen()),
    GetPage(name: userInfo, page: () => UserProfile()),
  ];

  static getnavbar() => navbar;
  static gethome() => home;
  static getphoneBook() => phoneBook;
  static getlinkRgt() => linkRgt;
  static getuserManagement() => userManagement;
  static getnotification() => notification;
  static getuserInfo() => userInfo;

  static String navbar = '/';
  static String home = '/home';
  static String phoneBook = '/phone-book';
  static String linkRgt = '/link-register';
  static String userManagement = '/user-management';
  static String notification = '/notification';
  static String userInfo = '/profile';
}