import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:namer_app/src/controller/Controller.dart';
import 'package:namer_app/src/screens/admin/link-registers/LinkRegisterScreen.dart';
import 'package:namer_app/src/screens/client/HomeScreen.dart';
import 'package:namer_app/src/screens/client/Profile.dart';
import 'package:namer_app/src/screens/client/notification/NotificationScreen.dart';
import 'package:namer_app/src/screens/client/phone-books/PhoneBookScreen.dart';

class NavbarScreen extends StatefulWidget {
  const NavbarScreen({super.key});

  @override
  State<NavbarScreen> createState() => _NavbarScreenState();
}

class _NavbarScreenState extends State<NavbarScreen> {
  final controller = Get.put(NavBarController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<NavBarController>(builder: (context) {
      return Scaffold(
        backgroundColor: Color(0xff000000),
        body: IndexedStack(
          index: controller.tabIndex,
          children: [
            HomeScreen(),
            PhoneBook(products: []),
            UserProfile(),
            NotificationScreen(notif: []),
            LinkRegister()
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.lightBlueAccent[200],
          unselectedItemColor: Colors.black,
          currentIndex: controller.tabIndex,
          onTap: controller.changeTabIndex,
          items: [
            _buttombarItem(Icons.home, "Home", "Go to Home page"),
            _buttombarItem(Icons.phone_android, "Phonebook", "Go to Phonebook page"),
            _buttombarItem(Icons.account_circle, "Profile", "Go to Profile page"),
            _buttombarItem(Icons.notifications_outlined, "Notification", "Go to Notification page"),
            _buttombarItem(Icons.app_registration_rounded, "Link Register", "Go to Register page"),
          ],
          ),
      );
    });
  }
}

// ignore: unused_element
_buttombarItem(IconData icon, String label, String tooltip){
  return BottomNavigationBarItem(icon: Icon(icon), label: label, tooltip: tooltip);
}